// Modules to control application life and create native browser window
const { app, BrowserWindow, protocol, systemPreferences, Menu } = require('electron');
const path = require('path');
const { startWebServer } = require('./startWebServer');


const PORT = 3000
let webServerRunning = false;

const isMac = process.platform === 'darwin';

protocol.registerSchemesAsPrivileged([
  { scheme: 'http', privileges: { standard: true, secure: true } }
]);


function createWindow() {
  const { menu } = require('./menu.js')

  Menu.setApplicationMenu(Menu.buildFromTemplate(menu))


  if (isMac) {
    systemPreferences.askForMediaAccess('camera').then(() => { console.log("OK camera") });
  }

  // Create the browser window.
  const editorWindow = new BrowserWindow({
    width: 1250,
    height: 900,
    minWidth: 800,
    minHeight: 600,
    show: false,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      //nativeWindowOpen: true
    }
  })

  const splash = new BrowserWindow({
    width: 600,
    height: 700,
    transparent: true,
    frame: false,
    alwaysOnTop: true
  });

  splash.loadFile(path.join(__dirname, '../renderer/splash.html'));
  splash.center();

  if (!webServerRunning) {
    startWebServer.startWebServer(PORT)
      .on('error', err => {
        editorWindow.loadFile("../renderer/error_init.html")
        setTimeout(() => { app.quit() }, 5000)
      })
      .on('listening', () => {
        // and load the index.html of the app.
        webServerRunning = true
        editorWindow.loadURL(`http://localhost:${PORT}/editor`, { extraHeaders: 'pragma: no-cache\n' })
        //scratchWindow.loadURL(`http://localhost:${PORT}/scratch`, { extraHeaders: 'pragma: no-cache\n' })
      })
  } else {
    editorWindow.loadURL(`http://localhost:${PORT}/editor`, { extraHeaders: 'pragma: no-cache\n' })
  }

  let counter = 0;
  editorWindow.webContents.on('did-create-window', (childWindow) => {
    const [ editorWindowX, editorWindowY ] = editorWindow.getPosition();
    x = editorWindowX + 10 + (counter%5)*10;
    y = editorWindowY + 10 + (counter%5)*10;
    counter++;
    childWindow.setPosition(x,y);
    // Para que se puedan cerrar las ventanas hijas con el aspa
    childWindow.webContents.on('will-prevent-unload', (e) => {
      e.preventDefault()
    })
  })
  
  setTimeout(() => {
    splash.close()
    editorWindow.show()
  }, 3000)

  // Open the DevTools.
  // editorWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
