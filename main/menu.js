const { app, BrowserWindow, Menu, Tray } = require('electron');
const { echidnalink } = require('echidnalink-lib');
const nativeImage = require('electron').nativeImage;
const path = require('path');

const isMac = process.platform === 'darwin';


let manualWindow = null;

function openManualWindow(pdf) {

  if (manualWindow) {
    manualWindow.close();
  }

  manualWindow = new BrowserWindow({
    width: 1250,
    height: 900,
    minWidth: 800,
    minHeight: 600,
    show: true,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })

  manualWindow.loadFile(path.join(__dirname, '../renderer/' + pdf + '.html'));

  manualWindow.focus();

  //manualWindow.webContents.openDevTools()

  manualWindow.on('closed', function () {
    manualWindow = null;
  })
}

function startEchidnaLink() {
  const iconPath = path.join(__dirname, '../images/echidna_40_24.png');
  const trayIcon = nativeImage.createFromPath(iconPath);

  var tray = new Tray(trayIcon);
  var server = echidnalink.startLink(tray);

  const contextMenu = Menu.buildFromTemplate([
    { label: 'Echidna conectado' },
    // { type: 'separator' },
    // {
    //   label: 'Salir', click() {
    //     tray.destroy();
    //     server.close();
    //   }
    // },
  ])

  tray.setToolTip('EchidnaLink activo');
  tray.setContextMenu(contextMenu);
}


const menu = [
  // { role: 'appMenu' }
  ...(isMac ? [{
    label: app.name,
    submenu: [
      { role: 'about' },
      { type: 'separator' },
      { role: 'services' },
      { type: 'separator' },
      { role: 'hide' },
      { role: 'hideOthers' },
      { role: 'unhide' },
      { type: 'separator' },
      { role: 'quit' }
    ]
  }] : []),

  //{ role: 'fileMenu' }
  {
    label: 'Archivo',
    submenu: [
      isMac ? { role: 'close' } : { role: 'quit' }
    ]
  },
  {
    label: 'Placas educativas',
    submenu: [
      {
        label: 'Echidna',
        click() { startEchidnaLink() }
      },
    ]
  },
  // { role: 'viewMenu' }
  {
    label: 'Aprende',
    submenu: [
      {
        label: 'Manual de LearningML',
        click() { openManualWindow('pdf_viewer') }
      },
    ]
  },
  {
    label: 'Actividades de ejemplo',
    submenu: [
      {
        label: 'Análisis de conducta',
        click() { openManualWindow('analisisconducta') }
      },
      {
        label: 'Juego de preguntas y respuestas',
        click() { openManualWindow('juego-preguntas-respuestas') }
      },
      {
        label: 'Asistente virtual',
        click() { openManualWindow('asistente-virtual') }
      },
      {
        label: 'Filtro de imágenes',
        click() { openManualWindow('filtro-de-imagenes') }
      },
      {
        label: 'Imitador',
        click() { openManualWindow('imitador') }
      }
    ]
  },
  {
    role: 'windowMenu'
  }

]

exports.menu = menu