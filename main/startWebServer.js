var staticServer = require('node-static');
var file = new staticServer.Server(`${__dirname}/../lml-services`)

function startWebServer(port) {
    return require('http').createServer(function (request, response) {
        request.addListener('end', function () {
            file.serve(request, response)
        }).resume()
    }).listen(port)
}

exports.startWebServer = { startWebServer }
