###
# v1.4.2 Primera versión de learningml-desktop
# |- learningml-desktop v1.4.2
# |- learningml-editor v1.4.2-desktop
# |- lml-scratch-vm v1.4.2-desktop
# `- lml-scratch-gui v1.4.2-desktop
#
# Para construirla definir las siguientes variables
# VERSION_DESKTOP=v1.4.2
# VERSION_EDITOR=v1.4.2-desktop
# VERSION_SCRATCH_GUI=v1.4.2-desktop
# VERSION_SCRATCH_VM=v1.4.2-desktop
###
# v1.5.0 Versión con extensión echidna
# |- learningml-desktop v1.5.0
# |- learningml-editor v1.5.0-desktop
# |- lml-scratch-vm v1.5.0-desktop
# `- lml-scratch-gui v1.5.0-desktop
# Para construirla definir las siguientes variables
# VERSION_DESKTOP=v1.5.0
# VERSION_EDITOR=v1.5.0-desktop
# VERSION_SCRATCH_GUI=v1.5.0-desktop
# VERSION_SCRATCH_VM=v1.5.0-desktop
###
# v1.6.0 Versión con Snap!
# |- learningml-editor v1.6.0-desktop
# |- lml-scratch-vm v1.6.0-desktop
# |- lml-scratch-gui v1.6.0-desktop
# `- lml-snap v1.6.0-desktop
# Para construirla definir las siguientes variables
# VERSION_EDITOR=v1.6.0-desktop
# VERSION_SCRATCH_GUI=v1.6.0-desktop
# VERSION_SCRATCH_VM=v1.6.0-desktop
# VERSION_SNAP=v1.6.0-desktop
###
# v1.7.0 Versión con Snap! y echidna
# |- learningml-editor v1.7.0-desktop
# |- lml-scratch-vm v1.7.0-desktop
# |- lml-scratch-gui v1.7.0-desktop
# `- lml-snap v1.7.0-desktop
# Para construirla definir las siguientes variables
# VERSION_EDITOR=v1.7.0-desktop
# VERSION_SCRATCH_GUI=v1.7.0-desktop
# VERSION_SCRATCH_VM=v1.7.0-desktop
# VERSION_SNAP=v1.7.0-desktop
###

EDITOR=1
SCRATCH=1
SNAP=1
VERSION_DESKTOP=v1.7.0
VERSION_EDITOR=v1.7.0-desktop
VERSION_SCRATCH_GUI=v1.7.0-desktop
VERSION_SCRATCH_VM=v1.7.0-desktop
VERSION_SNAP=v1.7.0-desktop

git checkout $VERSION_DESKTOP

if [[ $1 == "editor"  ]]
then
    rm -rf build editor
    mkdir build editor
    EDITOR=1
    SCRATCH=0
    SNAP=0
fi

if [[ $1 == "scratch"  ]]
then
    rm -rf build scratch
    mkdir build scratch
    EDITOR=0
    SCRATCH=1
    SNAP=0
fi

if [[ $1 == "snap"  ]]
then
    rm -rf build snap
    mkdir build snap
    EDITOR=0
    SCRATCH=0
    SNAP=1
fi

if [[ $1 == ""  ]]
then
    rm -rf build scratch editor
    mkdir build scratch editor
fi

cd build

# LearningML editor build
#########################

if [[ $EDITOR == 1 ]]
then    
    git clone https://gitlab.com/learningml/learningml-editor.git
    cd learningml-editor
    git checkout $VERSION_EDITOR
    npm install
    ng build --base-href /editor/ --configuration production
    mkdir -p ../../editor 
    mv dist/learningml/* ../../editor/
    cp ../../config.prod.json ../../editor/assets/config/config.prod.json
    cd ..
fi

# Scratch build
###############

if [[ $SCRATCH == 1 ]]
then
    git clone https://gitlab.com/learningml/lml-scratch-l10n
    git clone https://gitlab.com/learningml/lml-scratch-vm
    git clone https://gitlab.com/learningml/lml-scratch-gui

    cd lml-scratch-l10n
    npm install
    npm run build
    npm link
    cd ../lml-scratch-vm
    git checkout $VERSION_SCRATCH_VM
    npm install
    npm link
    npm link scratch-l10n
    cd ../lml-scratch-gui
    git checkout $VERSION_SCRATCH_GUI
    npm install
    npm link scratch-vm scratch-l10n

    npm run build
    mkdir -p ../../scratch
    mv build/* ../../scratch
fi


if [[ $SNAP == 1 ]]
then    
   git clone https://gitlab.com/learningml/lml-snap
   cd lml-snap
   git checkout $VERSION_SNAP
   mkdir -p ../../snap
   mv * ../../snap
fi