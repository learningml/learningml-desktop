# LearningML desktop

La estrategia para convertir LML en una aplicación de escritorio es la siguiente:

1. Construir el paquete learningml-editor (rama desktop)
2. Construir el paquete lml-scratch
3. El archivo main.js de electron se encarga de abrir un servidor que escucha en el puerto 3000.
4. La aplicación electron se encargará de cargar en pantalla el editor de ML o de Scratch, según lo solicite el usuario.

# Instalación para el desarrollo

- Clonar el repositorio.
- Construir las aplicaciones `learningml-editor` y `lml-scratch`. Este paso es opcional, pues el
  repositorio ya viene con una versión compilada de learningml-editor y lml-scratch.

Para construir las aplicaciones `learningml-editor` y `lml-scratch`: 
```
# cd lml-services
# ./build-services.sh
```

Se puede construir solo `learningml-editor` con:

```
# ./build-services.sh editor
```

O solo `lml-scratch` con

```
# ./build-services.sh scratch

```

- Instalar dependencias
```
# yarn install
```

-  Ejecutar electron
```
# yarn start
```

Nota: Si se vuelve a hacer una construcción de los servicios, es importante eliminar
la cache de la aplicación borrando el directorio Cache de la aplicación electron.

Windows:
C:\Users\<user>\AppData\Roaming\<yourAppName>\Cache

Linux:
/home/<user>/.config/<yourAppName>/Cache

OS X:
/Users/<user>/Library/Application Support/<yourAppName>/Cache

# Construcción de los instalables

Los instalables se construirán con `electron-builder` (https://www.electron.build/).

Para construir ejecutables/instalables, hay que ejecutar los siguientes comandos desde 
cada plataforma. Es decir, para construir los ejecutables de Linux, hay que lanzarlos desde una plataforma Linux, y lo mismo para los demás.

```bash
# yarn
# yarn dist
```

Para construir el paquete deb en Linux hay que instalar `binutils` y para construir el paquete 
rpm hay que instalar `rpm`.

En Linux se crea un directorio `dist` en cuyo interior se encuentra:

- Una aplicación autocontendida de tipo AppImage. Es muy útil para distribuirla, pues se trata de un solo archivo que puede ejecutarse directamente desde el navegador de archivos o desde una terminal.

- Un paquete debian

- Un directorio con el ejecutable y todas las dependencias necesarias para la ejecución de echidnalink.

# electron-quick-start

**Clone and run for a quick way to see Electron in action.**

This is a minimal Electron application based on the [Quick Start Guide](https://electronjs.org/docs/latest/tutorial/quick-start) within the Electron documentation.

**Use this app along with the [Electron API Demos](https://electronjs.org/#get-started) app for API code examples to help you get started.**

A basic Electron application needs just these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `main.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `index.html` - A web page to render. This is the app's **renderer process**.

You can learn more about each of these components within the [Quick Start Guide](https://electronjs.org/docs/latest/tutorial/quick-start).

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://github.com/electron/electron-quick-start
# Go into the repository
cd electron-quick-start
# Install dependencies
npm install
# Run the app
npm start
```

Note: If you're using Linux Bash for Windows, [see this guide](https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/) or use `node` from the command prompt.

## Resources for Learning Electron

- [electronjs.org/docs](https://electronjs.org/docs) - all of Electron's documentation
- [electronjs.org/community#boilerplates](https://electronjs.org/community#boilerplates) - sample starter apps created by the community
- [electron/electron-quick-start](https://github.com/electron/electron-quick-start) - a very basic starter Electron app
- [electron/simple-samples](https://github.com/electron/simple-samples) - small applications with ideas for taking them further
- [electron/electron-api-demos](https://github.com/electron/electron-api-demos) - an Electron app that teaches you how to use Electron
- [hokein/electron-sample-apps](https://github.com/hokein/electron-sample-apps) - small demo apps for the various Electron APIs

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
